import React, { useContext, useEffect, useState } from "react";
import { Button, Table } from "react-bootstrap";
import { Row } from "react-bootstrap";
import { Col } from "react-bootstrap";
import { Link } from "next/link";
import UserContext from "../../UserContext";
import View from "../../components/View";

export default function displaycategories() {
  return (
    <View title={"Categories"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Categories</h3>
          <Button
            className="mb-3"
            href="/categories"
            variant="success"
            size="lg"
          >
            Add
          </Button>
          <CategoryTable />
        </Col>
      </Row>
    </View>
  );
}

//this will add the category type and name in the category page
const CategoryTable = () => {
  const { user } = useContext(UserContext);
  return (
    <>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Category Name</th>
            <th>Category Type</th>
          </tr>
        </thead>
        <tbody>
          {user.id
            ? user.categories.map((category) => {
                return (
                  <tr key={category._id}>
                    <td>{category.categoryName}</td>
                    <td>{category.categoryType}</td>
                  </tr>
                );
              })
            : ""}
        </tbody>
      </Table>
    </>
  );
};
