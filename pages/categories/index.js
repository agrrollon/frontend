import React, { useContext, useState } from "react";
import { Button, Card, Col, Form } from "react-bootstrap";
import { Row } from "react-bootstrap";
import UserContext from "../../UserContext";
import View from "../../components/View";
import Router from "next/router";

export default function index() {
  return (
    <View title={"New Category"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>New Category</h3>
          <CategoryForm />
        </Col>
      </Row>
    </View>
  );
}
const CategoryForm = () => {
  const { user } = useContext(UserContext);
    console.log(user.id);
  const [categoryName, setCategoryName] = useState("");
  const [categoryType, setCategoryType] = useState("");

  function addCategory(e) {
    e.preventDefault();
    console.log(categoryName)
    console.log(categoryType)
    if (user.id) {
      fetch(`${AppHelper.API_URL}/users/addcategory`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          categoryName: categoryName,
          categoryType: categoryType
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);
          if (data === true) {
			//redirect to course page
			alert("Sucessfully added the category")
			Router.reload()
		} else {
			alert("Something went wrong.")
		}
        });
    }
  }
  
  return (
        <Form onSubmit={addCategory}>
          <Form.Group as={Row} controlId="categoryName">
            <Form.Label column sm={5}>
              Category Name
            </Form.Label>
            <Col sm={14}>
              <Form.Control 
              type="text" 
              value={categoryName} 
              onChange={e => setCategoryName(e.target.value)} 
              placeholder="Category Name" />
            </Col>
          </Form.Group>

          <Form.Group as={Row} controlId="categoryType">
            <Form.Label column sm={5}>
              Category Type
            </Form.Label>
            <Col sm={14}>
              <Form.Control 
              as="select" 
              onChange={e => setCategoryType(e.target.value)}>
                <option>Select Category Type</option>
                <option value='Income'>Income</option>
                <option value ='Expenses'>Expense</option>
              </Form.Control>
            </Col>
          </Form.Group>
          <Button type="submit">Submit</Button>
        </Form>
     
  );
};
