import { useState, useContext, useEffect } from "react";
import { Form, Button, Card, Row, Col } from "react-bootstrap";
import { GoogleLogin } from "react-google-login";
import Swal from "sweetalert2";
import UserContext from "../../UserContext";
import View from "../../components/View";
import AppHelper from "../../app-helper";
import Router from "next/router";


export default function index() {
  return (
    <View title={"Login"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Login</h3>
          <LoginForm />
        </Col>
      </Row>
    </View>
  );
}

const LoginForm = () => {
  const { user, setUser } = useContext(UserContext);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [isActive, setIsActive] = useState(false);

  //function to simulate user authentication
  function authenticate(e) {
    //prevent redirection via form submission
    e.preventDefault();

    const options = {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({
        email: email,
        password: password,
      }),
    };

    fetch(`${AppHelper.API_URL}/users/login`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(data);

        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          if (data.error === "does-not-exist") {
            Swal.fire("Authentication Failed", "User does not exist.", "error");
          } else if (data.error === "incorrect-password") {
            Swal.fire(
              "Authentication Failed",
              "Password is incorrect.",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered throug a different login procedure, try alternative login procedure.",
              "error"
            );
          }
        }
      });
  }

  const authenticateGoogleToken = (response) => {
    console.log(response.tokenId);
    const options = {
      method: "POST",
      headers: {
        "content-type": "application/json",
      },
      body: JSON.stringify({
        tokenId: response.tokenId,
      }),
    };

    fetch(`${AppHelper.API_URL}/users/verify-google-id-token`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(typeof data.accessToken);

        if (typeof data.accessToken !== "undefined") {
          localStorage.setItem("token", data.accessToken);
          retrieveUserDetails(data.accessToken);
        } else {
          if (data.error === "google-auth-error") {
            Swal.fire(
              "Google Auth Error",
              "Google authentication procedure failed",
              "error"
            );
          } else if (data.error === "login-type-error") {
            Swal.fire(
              "Login Type Error",
              "You may have registered through a different login procedure.",
              "error"
            );
          }
        }
      });
  };
  const retrieveUserDetails = (accessToken) => {
    const options = {
      headers: {
        Authorization: `Bearer ${accessToken}`,
      },
    };
    fetch(`${AppHelper.API_URL}/users/details`, options)
      .then(AppHelper.toJSON)
      .then((data) => {
        console.log(data); //to check lang to
        setUser({
          id: data.id,
          isAdmin: data.isAdmin,
        });
        Router.push('/')
      });
  };

  useEffect(() => {
    if (email !== "" && password !== "") {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password]);

  return (
    <Card>
      <Card.Header>Login Details</Card.Header>
      <Card.Body>
        <Form onSubmit={authenticate}>
          <Form.Group controlId="userEmail">
            <Form.Label>Email Address</Form.Label>
            <Form.Control
              type="email"
              value={email}
              onChange={(e) => setEmail(e.target.value)}
              autoComplete="off"
              required
            />
          </Form.Group>
          <Form.Group controlId="password">
            <Form.Label>Password</Form.Label>
            <Form.Control
              type="password"
              value={password}
              onChange={(e) => setPassword(e.target.value)}
              required
            />
          </Form.Group>
          <Button className="mb-2 bg-success" type="submit" block>
            Login
          </Button>

          <GoogleLogin
            clientId="529678454482-kuijd2fp9r549gr2a8kbac9vhqffthdo.apps.googleusercontent.com"
            buttonText="Login"
            onSuccess={authenticateGoogleToken}
            onFailure={authenticateGoogleToken}
            cookiePolicy={"single_host_origin"}
            className="w-100 text-center d-flex justify-content-center"
          />
        </Form>
      </Card.Body>
    </Card>
  );
};

/*

    Google Login Cycle:
        Popup
        Select Account
        Google API Response (Token)
        Use Reponse.TokenId fron Google API to authenticateAPI - Response

 */
