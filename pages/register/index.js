import { useState, useEffect } from "react";
import { Form, Button } from "react-bootstrap";
import Router from "next/router";

export default function index() {
  // Form input state hooks
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [email, setEmail] = useState("");
  const [mobileNo, setMobileNo] = useState("");
  const [password1, setPassword1] = useState("");
  const [password2, setPassword2] = useState("");

  // State to determine whether submit button is enabled or not
  const [isActive, setIsActive] = useState(false);

  // Function to simulate user registration
  function registerUser(e) {
    e.preventDefault();
    if (
      password1 !== "" &&
      password2 !== "" &&
      password2 === password1 &&
      mobileNo.length === 11
    ) {
      //check for duplicate email in database first
      fetch(`${AppHelper.API_URL}/users/email-exists`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          email: email,
        }),
      })
        .then((res) => res.json())
        .then((data) => {
          console.log(data);

          //if no duplicates found
          if (data === false) {
            fetch(`${AppHelper.API_URL}/users/`, {
              method: "POST",
              headers: {
                "Content-Type": "application/json",
              },
              body: JSON.stringify({
                firstName: firstName,
                lastName: lastName,
                email: email,
                password: password1,
                mobileNo: mobileNo,
              }),
            })
              .then((res) => res.json())
              .then((data) => {
                console.log(data);

                if (data === true) {
                  //Registration successful
                  setFirstName("");
                  setLastName("");
                  setEmail("");
                  setMobileNo("");
                  setPassword1("");
                  setPassword2("");
                  alert("registered successfuly");
                  //redirect to login
                  Router.push('/login')
                } else {
                  alert("Something went wrong");
                }
              })
              .catch((error) => {
                console.error("Error:", error);
              });
          }
        });
    } else {
      alert("Please check the information provided");
    }

    // Clear input fields


    console.log("Thank you for registering!");
    alert("Thank you for registering!");
  }

  // Validate form input whenever email, password1, or password2 is changed
  useEffect(() => {
    // Validation to enable submit button when all fields are populated and both passwords match
    if (
      email !== "" &&
      password1 !== "" &&
      password2 !== "" &&
      password2 === password1
    ) {
      setIsActive(true);
    } else {
      setIsActive(false);
    }
  }, [email, password1, password2]);

  return (
    <Form onSubmit={(e) => registerUser(e)}>
      <Form.Group controlId="firstName">
        <Form.Label>First Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="First Name"
          value={firstName}
          onChange={(e) => setFirstName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="lastName">
        <Form.Label>Last Name</Form.Label>
        <Form.Control
          type="text"
          placeholder="Last Name"
          value={lastName}
          onChange={(e) => setLastName(e.target.value)}
          required
        />
      </Form.Group>
      <Form.Group controlId="userEmail">
        <Form.Label>Email address</Form.Label>
        <Form.Control
          type="email"
          placeholder="Enter email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          required
        />
        <Form.Text className="text-muted">
          We'll never share your email with anyone else.
        </Form.Text>
      </Form.Group>
      <Form.Group controlId="mobileNumber">
        <Form.Label>Mobile Number</Form.Label>
        <Form.Control
          type="text"
          placeholder="Mobile Number"
          value={mobileNo}
          onChange={(e) => setMobileNo(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password1">
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Password"
          value={password1}
          onChange={(e) => setPassword1(e.target.value)}
          required
        />
      </Form.Group>

      <Form.Group controlId="password2">
        <Form.Label>Verify Password</Form.Label>
        <Form.Control
          type="password"
          placeholder="Verify Password"
          value={password2}
          onChange={(e) => setPassword2(e.target.value)}
          required
        />
      </Form.Group>

      {/* Conditionally render submit button based on isActive state */}
      {isActive ? (
        <Button variant="primary" type="submit" id="submitBtn">
          Submit
        </Button>
      ) : (
        <Button variant="primary" type="submit" id="submitBtn" disabled>
          Submit
        </Button>
      )}
    </Form>
  );
}
