import React, { useContext, useEffect, useState } from "react";
import { Button, Card, Col, Form } from "react-bootstrap";
import { Row } from "react-bootstrap";
import View from "../../components/View";
import UserContext from "../../UserContext";
import Router from "next/router";

export default function addrecord() {
  return (
    <View title={"Add Record"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Add New Record</h3>
          <AddNewRecord />
        </Col>
      </Row>
    </View>
  );
}

const AddNewRecord = () => {
  const { user } = useContext(UserContext);
  const [categories, setCategories] = useState("");
  const [categoryType, setCategoryType] = useState("");
  const [categoryName, setCategoryName] = useState("");
  const [amount, setAmount] = useState(0);
  const [description, setDescription] = useState("");

  function addrecordlist(e) {
    e.preventDefault();
    console.log(user.id);
    // console.log(categoryName)
    // console.log(categoryType)
    if (user.id) {
      console.log(user.id);
      fetch(`${AppHelper.API_URL}/users/addrecord`, {
        method: "POST",
        headers: {
          "Content-type": "application/json",
          Authorization: `Bearer ${localStorage.getItem("token")}`,
        },
        body: JSON.stringify({
          categoryName: categoryName,
          categoryType: categoryType,
          amount: amount,
          description: description,
        }),
      })
        .then(res => res.json())
        .then(data => {
          console.log(data);
          if (data === true) {
            alert("Sucessfully added the record");
            Router.reload()
          } else {
            alert("Something went wrong.");
          }
        });
    }
  }

  useEffect(() => {
    console.log(user.id);
    fetch(`${AppHelper.API_URL}/users/details`, {
      headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${localStorage.getItem("token")}`,
      },
    })
      .then((res) => res.json())
      .then((data) => {
        // console.log(data.categories)
        // console.log(categoryType)
        if (categoryType === "Expenses") {
          const categoryList = data.categories.map((category) => {
            if (category.categoryType === "Expenses") {
              console.log(category.categoryName);
              console.log(category._id);
              return (
                <option key={category._id}>{category.categoryName}</option>
              );
            }
          });
          setCategories(categoryList);
        }
        if (categoryType === "Income") {
          const categoryList = data.categories.map((element) => {
            if (element.categoryType === "Income") {
              return <option key={element._id}>{element.categoryName}</option>;
            }
          });
          setCategories(categoryList);
        }
      });
  }, [categoryType]);

  return (
    <Card >
      <Card.Header as="h5">Record Information</Card.Header>
      <Card.Body>
        <Card.Text>Category Type</Card.Text>
        <Form onSubmit={addrecordlist}>
          <Form.Control
            as="select"
            value={categoryType}
            onChange={(e) => setCategoryType(e.target.value)}
          >
            <option default>Select Category</option>
            <option value="Income">Income</option>
            <option value="Expenses">Expense</option>
          </Form.Control>
          <br />
          <Card.Text>Category Name</Card.Text>
          <Form.Control
            as="select"
            onChange={(e) => setCategoryName(e.target.value)}
          >
            {categories}
          </Form.Control>
          <br />
          <Card.Text>Amount</Card.Text>
          <Form.Control
            type="number"
            value={amount}
            onChange={(e) => setAmount(e.target.value)}
            placeholder="Amount"
          />
          <br />
          <Card.Text>Description</Card.Text>
          <Form.Control
            type="text"
            value={description}
            onChange={(e) => setDescription(e.target.value)}
            placeholder="Enter Description"
          />
          <Button className="mt-3" variant="success" type="submit">
            Add Record
          </Button>
        </Form>
      </Card.Body>
    </Card>
  );
};
