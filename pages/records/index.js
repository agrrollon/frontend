import moment from "moment";
import React, { useContext, useEffect, useState } from "react";
import { Card, InputGroup } from "react-bootstrap";
import { FormControl } from "react-bootstrap";
import { Col, Button, Form } from "react-bootstrap";
import { Row } from "react-bootstrap";
import View from "../../components/View";
import UserContext from "../../UserContext";

export default function index() {
  return (
    <View title={"Records"}>
      <Row className="justify-content-center">
        <Col xs md="6">
          <h3>Records </h3>
          <RecordDisplay />
        </Col>
      </Row>
    </View>
  );
}

const RecordDisplay = () => {
  const { user } = useContext(UserContext);
  const [categoryType, setCategoryType] = useState("");
  const [searchTerm, setSearchTerm] = useState("");
  const [records, setRecords] = useState([]);
  //const [amount, setAmount] = useState("");
  const [totalBalance, setTotalBalance] = useState(0);
  
 let balance = 0;

  useEffect(() => {
      if(user.id){
          for (let record of user.records){
              if(record.categoryType === "Income"){
                balance = balance + record.amount
              } else if(record.categoryType === "Expenses"){
                  balance = balance - record.amount
              }
          }
          setTotalBalance(balance)
      }
  },[totalBalance, searchTerm, categoryType])


  useEffect(() => {
    if (user.id) {
      const recordList = user.records.map((record) => {
        if (categoryType === "All") {
          if (
            record.categoryName.toLowerCase().includes(searchTerm.toLowerCase())
          ) {
            return (
              <Card className="mb-3" key={record._id}>
                <Card.Header>
                  <strong>Description: {record.description}</strong>
                </Card.Header>
                <Card.Body>
                  <Card.Text>{record.categoryType}</Card.Text>
                  <Card.Text>
                    Date: {moment(records.createdOn).format("DD-MM-YYYY")}
                  </Card.Text>
                  <Card.Text>Amount: {record.amount}</Card.Text>
                  {/* <Card.Text>Remaining Balance: {amount}</Card.Text> */}
                </Card.Body>
              </Card>
            );
          }
        } else if (categoryType === "Income") {
          if (record.categoryType === "Income") {
            if (
              record.categoryName
                .toLowerCase()
                .includes(searchTerm.toLowerCase())
            ) {
              return (
                <Card className="mb-3" key={record._id}>
                  <Card.Header>
                    <strong>Description: {record.description}</strong>
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>{record.categoryType}</Card.Text>
                    <Card.Text>
                      Date: {moment(records.createdOn).format("DD-MM-YYYY")}
                    </Card.Text>
                    <Card.Text>Amount: {record.amount}</Card.Text>
                    {/* <Card.Text>Remaining Balance:</Card.Text> */}
                  </Card.Body>
                </Card>
              );
            }
          }
        } else if (categoryType === "Expenses") {
          if (record.categoryType === "Expenses") {
            if (
              record.categoryName
                .toLowerCase()
                .includes(searchTerm.toLowerCase())
            ) {
              return (
                <Card className="mb-3" key={record._id}>
                  <Card.Header>
                    <strong>Description: {record.description}</strong>
                  </Card.Header>
                  <Card.Body>
                    <Card.Text>{record.categoryType}</Card.Text>
                    <Card.Text>
                      Date: {moment(records.createdOn).format("DD-MM-YYYY")}
                    </Card.Text>
                    <Card.Text>Amount: {record.amount}</Card.Text>
                    {/* <Card.Text>Remaining Balance:</Card.Text> */}
                  </Card.Body>
                </Card>
              );
            }
          }
        }
      });
      setRecords(recordList);
    }
  }, [categoryType, searchTerm]);
  return (
    <>
      <InputGroup className="mb-3">
        <InputGroup.Prepend>
          <Button href="records/addrecord" variant="success">
            Add Record
          </Button>
        </InputGroup.Prepend>
        <FormControl
          aria-describedby="basic-addon1"
          type="text"
          placeholder="Search Records"
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <Form.Control
          as="select"
          onChange={(e) => setCategoryType(e.target.value)}
        >
          <option value="All">All</option>
          <option value="Income">Income</option>
          <option value="Expenses">Expenses</option>
        </Form.Control>
      </InputGroup>
      <>
        <h3>Running Balance: {totalBalance} </h3>
        {records}
      </>
    </>
  );
};
